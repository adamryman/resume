## Skills
- Languages: Go (4 years), PHP (3 year), Bash (5 years), Java, Python, Javascript, HTML, CSS, C, C#, Verilog
- CI/CD for several teams. Gitlab-CI, Jenkins, Circle-CI, Travis-CI.
- AWS ECS, EC2, ELB/ALB, S3, Route53, EMR, etc
- Utilizing Gitlab, Terraform, ELK, Jira, Confluence

## Experience

### - Software Development Engineer

\-- ***Tune && Branch*** | *Seattle, WA* | 2016 - 2019

- **Software Engineer - Branch Migration**
	- Facilitated seamless experience for customers transitioning to Branch by implementing tooling for migrating webhook templates between Tune's and Branch's domain specific languages.
	- Ensured identical customer experience by automating Tune webhook exports using CI/CD tooling for data comparisons.
	- Responsible for mirroring of billions of requests per day with less than 0.1% error rate through both Branch and Tune systems.
- **SDE II - Tune Data Core**
	- Reduced cost by migrating production services from over-previsioned reserved hardware to autoscaling Amazon ECS.
	- Improved customer satisfaction by hunting down and fixing customer issues in multi-service pipelines.
	- Contributed to reliability by being on-call for 30K rps distributed system with regular operational overviews.
	- Improved developer productivity by decreasing time to complete CI/CD jobs for build, test, and deploy utilizing caching, docker image optimization, and multi stage pipelines.
- **SDE I - Tune Data Core**
	- Increased development throughput by containerizing dev environment for services and infrastructure in Go & PHP.
	- Aligned terminology within engineering teams with documentation and classes.
- **SDE I - Tune Research & Development**
	- Implemented initial CI/CD for build and test on PR, deploying to staging, and deploying to production.
	- Rapidly created new services utilizing **truss**.
	- Scaled new multi-service product from 1 host to autoscaling on Amazon ECS.
- **Intern - Tune Research & Development**
	- Decreased development time by co-authoring **truss**, a code generation tool.

### - Co-Creator && Key Contributor

\-- ***truss*** | *[github.com/metaverse/truss](https://github.com/metaverse/truss)* | 2016 - Current

- *golang*, *protobuf/gRPC*.
- Rapidly build gRPC&HTTP 1.1/JSON services by generating messages and encode/decode logic from protobuf/gRPC definitions.
- Production usage at Tune and Glympse.
- Active contributions through pull requests and issues.

### - Chief Operations Officer

\-- ***StudentRND - 501c3*** | *Bellevue, WA* | 2010 - 2013

- Networked with professionals in industry to develop resources for students to design, manufacture, test, & sell experimental products.
- Grew programming marathon, CodeDay, from grassroots to national event.
	- 2011: First event.
	- 2012: First out of Washington; in Portland, OR.
	- 2013: Events in 5 major cities (Seattle, Portland, San Francisco, Los Angeles, and New York City).
    - Since 2011, more than 35,000 students have attended over 350 CodeDays across the US and Canada.
- Raised over $50k to operate non-profit endeavors.
- Organized regular workshops teaching 30 or more students how to design & build things using laser cutters, 3D printers, electronic circuity, & various SDKs.


## Education

### Electrical Engineering B.S.
 ***University of Washington*** | *Seattle, WA* | 2015

### Internships

- **Software Engineering Intern** (SDK/Java/Node/C#)
	- ***Splunk*** | *Seattle, WA* | June 2015 - Sept 2015
- **Web Application Intern** (Ruby on Rails)
	- ***Tyemill*** | *Seattle, WA* | June 2014 - Sept 2014
- **Web Application Developer** (D3)
	- ***BlackRock*** | *Seattle, WA* | Nov 2013 - June 2014
- **Research Intern** (Python, Java, Weka)
	- ***PNNL*** | *Richland, WA* | June 2011 - Aug 2011

